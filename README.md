# Entity UI Builder

The Entity UI Builder module provides an admin UI for creating local tasks
(tabs) on content entities. Tabs can contain anything; their output is created
by different plugins. This module contains the following tab content plugins:

- entity forms, showing a given form mode
- entity display, showing a given view mode
- forms for configurable actions, allowing the action to be executed
  immediately  by the user.
- form to change the owner of the entity.

## Requirements

This module requires no modules outside of Drupal core.

- For dragging of entity tabs to work correctly with locked items, the patch for
  Drupal core from https://www.drupal.org/project/drupal/issues/3388503 is
  required. Note that this patch will allow an item to be dragged in between two
  locked items which have the same weight, but on save the dragged item will be
  positioned after all the locked items with the same weight. Without this
  patch, you can show row weights and use the weight form elements directly.

## Installation

Install as you would normally install a contributed Drupal module.
For further information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).

## Configuration

The UI for configuring tabs for an entity type is alongside its existing admin
UI:

- Entity types which use bundles get an extra tab beside the bundles list.
  For example, entity tabs on nodes can be configured at
  admin/structure/types/entity_ui.
- Entity types which do not use bundles but are fieldable get an extra tab
  alongside their field admin UI.

## Maintainers

- Joachim Noreiko - [joachim](https://www.drupal.org/u/joachim)
